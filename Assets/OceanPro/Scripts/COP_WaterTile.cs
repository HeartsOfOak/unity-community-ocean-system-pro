using UnityEngine;

[ExecuteInEditMode]
public class COP_WaterTile : MonoBehaviour 
{
	public COP_PlanarReflection reflection;
	public COP_WaterBase waterBase;
	
	public void Start () 
	{
		AcquireComponents();
	}
	
	private void AcquireComponents() 
	{
		if (!reflection) {
			if (transform.parent)
				reflection = (COP_PlanarReflection)transform.parent.GetComponent<COP_PlanarReflection>();
			else
				reflection = (COP_PlanarReflection)transform.GetComponent<COP_PlanarReflection>();	
		}
		
		if (!waterBase) {
			if (transform.parent)
				waterBase = (COP_WaterBase)transform.parent.GetComponent<COP_WaterBase>();
			else
				waterBase = (COP_WaterBase)transform.GetComponent<COP_WaterBase>();	
		}
	}
	
#if UNITY_EDITOR
	public void Update () 
	{
		AcquireComponents();
	}
#endif
	
	public void OnWillRenderObject() 
	{
		if (reflection)
			reflection.WaterTileBeingRendered(transform, Camera.current);
		if (waterBase)
			waterBase.WaterTileBeingRendered(transform, Camera.current);		
	}
}
