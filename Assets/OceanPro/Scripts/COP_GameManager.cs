﻿using UnityEngine;
using System.Collections;

public class COP_GameManager : MonoBehaviour {


	public COP_Ocean ocean;
	public COP_Wind wind;

	//fps counter
	private bool isEnabled = false;
	private double fps;
	public float updateInterval = 0.5F;
	private double lastInterval;
	private int frames = 0;
	//public WindZone localWind;

	protected void OnEnable()
	{

	}
	 
	// Use this for initialization
	void Start () {
        Screen.showCursor = false;

		lastInterval = Time.realtimeSinceStartup;
		frames = 0;
	}

	void Awake() {
	}
	
	// Update is called once per frame
	void Update () {

		//increase or decrease the fixed update time
		if (Input.GetKeyUp (KeyCode.F3)) {
			Time.fixedDeltaTime = Time.fixedDeltaTime - 0.01f;
			Debug.Log ("fixedDeltaTime: " + Time.fixedDeltaTime);
		}
		if (Input.GetKeyUp (KeyCode.F4)) {
			Time.fixedDeltaTime = Time.fixedDeltaTime + 0.01f;
			Debug.Log ("fixedDeltaTime: " + Time.fixedDeltaTime);
		}


//        if (Input.GetButtonDown("ResetLevel"))
//        {
//            Application.LoadLevel(0);
//        }
//        if (Input.GetButtonDown("Exit Game"))
//        {
//            Application.Quit();
//        }

		if (wind != null && ocean != null) {
			if (Input.GetKey (KeyCode.F6)) {
				ocean.transform.Rotate(Vector3.up,1);
			}

			if (Input.GetKey (KeyCode.F5)) {
				ocean.transform.rotation = Quaternion.Euler(0,0,0);	
			}

			if (Input.GetKey (KeyCode.F12)) {
			wind.waveScale += .1f;
			}

			if (Input.GetKey (KeyCode.F11)) {
				if (wind.waveScale > 0.1) {
					wind.waveScale -= .1f;
				};
			}
			if (Input.GetKey (KeyCode.F10)) {
					ocean.choppy_scale += .1f;
			}

			if (Input.GetKey (KeyCode.F9)) {
				if (ocean.choppy_scale > 0.1) {
						ocean.choppy_scale -= .1f;
				};
			}

			if (Input.GetKey (KeyCode.F8)) {
				ocean.speed += .01f;
			}

			if (Input.GetKey (KeyCode.F7)) {
				if (ocean.speed >= 0.05f) {
						ocean.speed -= 0.01f;
				};
			}
		}




		if (Input.GetKeyUp (KeyCode.F1)) {
			isEnabled = !isEnabled;
		}
		if (isEnabled) {
						//FPS code
						++frames;
						double timeNow = Time.realtimeSinceStartup;
						if (timeNow > lastInterval + updateInterval) {
								fps = frames / (timeNow - lastInterval);
								frames = 0;
								lastInterval = timeNow;
						}
				}

		//check the wind, see if it has been going the same direction for any length of time
		//if (sky.Components.Weather.Weather == TOD_Weather.WeatherType.Storm) {
		//				wind.forceStorm = true;
		//				ocean.choppy_scale = 9;
		//		} else {
		//	ocean.choppy_scale = 3;	
		//}
	}

	void OnGUI() {
		if (isEnabled) {
			GUILayout.Label("" + fps.ToString("f2"));		
		}
	}
}
