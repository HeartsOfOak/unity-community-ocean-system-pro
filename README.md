### Unity Community Ocean System (Pro Version) ###

* Designed for Unity Pro
* Fast
* Lightweight
* Fully customizable

### Features ###

* Wave height, wave speed, wave pitch adjustable
* Adjustable wave foam
* Adjustable shoreline foam
* Water distortion and reflection
* Buoyancy script included
